//Adem VAROL - 200709078

import java.text.DecimalFormat;

public class TestCircle {
    public static void main(String[] args) {
        Circle circleA = new Circle(3, new Point(10, 10));
        System.out.println("Area of circle      = " + new DecimalFormat("##.00").format(circleA.Area()));
        System.out.println("Perimeter of circle = " + new DecimalFormat("##.00").format(circleA.Perimeter()));

        Circle circleB = new Circle(2, new Point(13, 14));
        int Intersect = circleA.Intersect(circleB);
        if (Intersect == -1) {
            System.out.println("Circles do intersecting...");
        } else if (Intersect == 0) {
            System.out.println("Circles tangent to each other...");
        } else {
            System.out.println("Circles don't intersecting...");
        }
    }
}
