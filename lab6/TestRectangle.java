//Adem VAROL - 200709078
public class TestRectangle {
    public static void main(String[] args) {

        Point topLeft = new Point(10, 10);
        Rectangle Rect = new Rectangle(5, 6, topLeft);

        System.out.println("Area of rectangle      = " + Rect.Area());
        System.out.println("Perimeter of rectangle = " + Rect.Perimeter());

        Point[] Points = Rect.Corners();
        Character chr = 'A';
        for (int i = 0; i < Points.length; i++) {
            System.out.println("Coordinate of corner " + (chr++) + " = (" + Points[i].xCoord + "," + Points[i].yCoord + ")");
        }
    }
}
