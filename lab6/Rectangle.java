//Adem VAROL - 200709078
public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }

    public int Area() {
        return (sideA * sideB);
    }

    public int Perimeter() {
        return (2 * (sideA + sideB));
    }

    public Point[] Corners() {

        Point[] Corners = new Point[4];
        Corners[0] = topLeft; //TopLeft
        Corners[1] = new Point(topLeft.xCoord, topLeft.yCoord - sideA); //BottomLeft
        Corners[2] = new Point(topLeft.xCoord + sideB, topLeft.yCoord - sideA); //BottomRight
        Corners[3] = new Point(topLeft.xCoord + sideB, topLeft.yCoord); //TopRight

        return Corners;
    }
}