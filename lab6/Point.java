//Adem VAROL - 200709078
public class Point {
    int xCoord;
    int yCoord;

    public Point(int xCoord, int yCoord) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public double DistanceFromAPoint(Point point) {
        double xD = xCoord - point.xCoord;
        double yD = yCoord - point.yCoord;
        return Math.sqrt(Math.pow(xD, 2) + Math.pow(yD, 2));
    }
}
