//Adem VAROL - 200709078
public class Circle {
    int Radius;
    Point Center;

    public Circle(int radius, Point center) {
        Radius = radius;
        Center = center;
    }

    public double Area() {
        return (Math.PI * Math.pow(Radius, 2));
    }

    public double Perimeter() {
        return (2 * Math.PI * Radius);
    }

    public int Intersect(Circle circle) {
        if ((Radius + circle.Radius) < (Center.DistanceFromAPoint(circle.Center))) {
            return 1;
        } else if ((Radius + circle.Radius) == (Center.DistanceFromAPoint(circle.Center))) {
            return 0;
        } else {
            return -1;
        }
    }
}
