//Adem VAROL - 200709078
package shapes;

public class Square extends Shape {
    double side;
    public Square(double side) {
        this.side = side;
    }

    public double area() {
        return side * side;
    }
}
