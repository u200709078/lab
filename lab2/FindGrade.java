//Adem VAROL - 200709078
public class FindGrade {
        public static void main(String[] args){
	if (args.length==1){

            int score = Integer.parseInt(args[0]);
            String grd = "";

		if (90 <= score && score <= 100) {
			grd = "Your grade is A";
		}else if (80 <= score && score < 90) {
                	grd = "Your grade is B";
		}else if (70 <= score && score < 80) {
                	grd = "Your grade is C";
		}else if (60 <= score && score < 70) {
                	grd = "Your grade is D";
		}else if (0 <= score && score < 60) {
                	grd = "Your grade is F";
		}else{
			grd = "It is not a valid score!";
		}
		System.out.println(grd);

	}else{
	System.out.println("You need to enter one value!");
	}

        }
}
