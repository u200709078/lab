//Adem VAROL - 200709078
public class CompareNumbers {

	public static void main(String[] args){

		/*
		int value1 = 4;
		int value2 = 9;
		Execute: 4 < 9 */

		/* 4 < 9
		int value1 = 4;
		int value2 = 2;
		Execute: 4 > 2*/

		/* 4 < 9
		int value1 = 2;
		int value2 = 2;
		Execute: 2 = 2*/

		int value1 = 2;
		int value2 = 2;
		
		if (value1 < value2) {
			System.out.println(value1 + " < " + value2); // 4 < 9
		}else if (value1 == value2) {
			System.out.println(value1 + " = " + value2);
		}else {
			System.out.println(value1 + " > " + value2);
		}

	}
}