package demo;

import stack.Stack;
import stack.StackArrayImpl;
import stack.StackImpl;

public class StackDemo {
    public static void main(String[] args) {
        //Stack stack = new StackImpl();
        Stack stack = new StackArrayImpl();
        stack.push("A");
        stack.push("D");
        stack.push("E");
        stack.push("M");
        stack.push("Hello");

        while (!stack.empty()) {
            System.out.println(stack.pop());
        }
    }
}
