//Adem VAROL  200709078
package shapes3d;
import shapes2d.Square;

public class Cube extends Square {
    public Cube (double Side){
        super(Side);
    }

    public double Volume(){
        return super.Area()*Side;
    }

    @Override
    public double Area(){
        return 6*super.Area();
    }
}
