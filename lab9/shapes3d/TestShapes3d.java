//Adem VAROL - 200709078
package shapes3d;

import javax.crypto.spec.PSource;
import java.text.DecimalFormat;

public class TestShapes3d {
    public static void main(String[] args) {
        Cylinder cy = new Cylinder(5, 10);
        System.out.println(cy);
        System.out.println("Cylinder Area   = "+new DecimalFormat("##.00").format(cy.Area()));
        System.out.println("Cylinder Volume = "+new DecimalFormat("##.00").format(cy.Volume()));

        Cube cb=new Cube(4);
        System.out.println(cb);
        System.out.println("Cube Volume     = "+cb.Volume());
        System.out.println("Cube Area       = "+cb.Area());
    }

}
