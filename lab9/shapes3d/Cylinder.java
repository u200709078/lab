//Adem VAROL - 200709078
package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    private double Height;

    public Cylinder(double Radius, double Height) {
        super(Radius);
        this.Height = Height;
    }

    @Override
    public double Area() {
        return 2 * super.Area() + 2 * Math.PI * Radius * Height;
    }

    public double Volume() {
        return Height * super.Area();
    }

    @Override
    public String toString() {
        return "Cylinder Height = " + Height + ", " + super.toString();
    }
}
