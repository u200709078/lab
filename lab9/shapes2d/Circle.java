//Adem VAROL - 200709078
package shapes2d;

public class Circle extends Object {
    protected double Radius;

    public Circle(double Radius) {
        this.Radius = Radius;
    }

    public double Area() {
        return Math.PI * Math.pow(Radius, 2);
    }

    @Override
    public String toString() {
        return "Circle Radius   = " + Radius;
    }
}
