//Adem VAROL - 200709078
package shapes2d;

import java.lang.Object;

public class Square extends Object {
    protected double Side;

    public Square(double Side) {
        this.Side = Side;
    }

    public double Area() {
        return Math.pow(Side, 2);
    }

    @Override
    public String toString() {
        return "Square Side     = " + Side;
    }
}
