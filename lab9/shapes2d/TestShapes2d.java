//Adem VAROL - 200709078
package shapes2d;

import java.text.DecimalFormat;

public class TestShapes2d {
    public static void main(String[] args) {
        Circle cr = new Circle(5);
        Square sq = new Square(4);

        System.out.println("Circle Area     = " + new DecimalFormat("##.00").format(cr.Area()));
        System.out.println(cr);
        System.out.println(sq);
    }
}
