//Adem VAROL - 200709078

import java.util.Scanner;

public class GCDLoop {
    public static void main(String[] args) {
        Scanner rdr = new Scanner(System.in);
        int a, b, swp, GCD;

        while (true) {
            System.out.print("Enter first number:");
            a = rdr.nextInt();
            System.out.print("Enter second number:");
            b = rdr.nextInt();
            if (a <= 0 || b <= 0) {
                System.out.println("\nPlease enter positive numbers...");
            } else {
                break;
            }
        }

        if (a < b) {
            swp = a;
            a = b;
            b = swp;
        }
        GCD = EuclidAlg(a, b);
        System.out.println("\nGCD(" + a + "," + b + ") = " + GCD);
    }

    public static int EuclidAlg(int a, int b) {
        int k;
        if (a % b == 0) {
            return b;
        }
        while (true) {
            k = a;
            a = b;
            b = k % a;
            if (a % b == 0) {
                return b;
            }
        }
    }
}
