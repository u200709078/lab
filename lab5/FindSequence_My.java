//Adem VAROL - 200709078
//Bunda dizinin değişimini yapamadım.

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence_My {

    public static void main(String[] args) throws FileNotFoundException {
        int matrix[][] = readMatrix();
        printMatrix(matrix);
        boolean found = false;
        search:
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (search(0, matrix, i, j)) {
                    found = true;
                    break search;
                }
            }
        }

        if (found) {
            System.out.println("\nA sequence is found");
        }

    }

    private static boolean search(int cNumber, int[][] matrix, int row, int col) {
        int upNumber = 0, downNumber = 0, leftNumber = 0, rightNumber = 0;
        if (cNumber == 9) {
            //System.out.println("Row="+row+" Col="+col+" cNumber="+cNumber);
            return true;
        }
        if (cNumber == matrix[row][col]) {
            //System.out.println("Row="+row+" Col="+col+" cNumber="+cNumber);
            if (row > 0) upNumber = matrix[row - 1][col];
            if (row < matrix.length-1) downNumber = matrix[row + 1][col];
            if (col > 0) leftNumber = matrix[row][col - 1];
            if (col < matrix.length-1) rightNumber = matrix[row][col + 1];
            if (upNumber == cNumber + 1) {
                cNumber++;
                return search(cNumber, matrix, row - 1, col);
            } else if (downNumber == cNumber + 1) {
                cNumber++;
                return search(cNumber, matrix, row + 1, col);
            } else if (leftNumber == cNumber + 1) {
                cNumber++;
                return search(cNumber, matrix, row, col - 1);
            } else if (rightNumber == cNumber + 1) {
                cNumber++;
                return search(cNumber, matrix, row, col + 1);
            }
        }
        return false;
    }

    private static int[][] readMatrix() throws FileNotFoundException {
        int[][] matrix = new int[10][10];
        File file = new File("matrix.txt"); // for command line
        //File file = new File("lab5/matrix.txt"); // for IntelliJ
        try (Scanner sc = new Scanner(file)) {

            int i = 0;
            int j = 0;
            while (sc.hasNextLine()) {
                int number = sc.nextInt();
                matrix[i][j] = number;
                if (j == 9)
                    i++;
                j = (j + 1) % 10;
                if (i == 10)
                    break;
            }
        } catch (FileNotFoundException e) {
            throw e;
        }
        return matrix;
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
