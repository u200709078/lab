//Adem VAROL - 210709078

import java.io.IOException;
import java.util.Scanner;

public class TicTacToe_E2 {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
        int row, col;
        boolean player1 = true, player2 = true, gameend = true, winner;

        System.out.println("WELCOME TO TIC-TAC-TOE\n");

        System.out.println("Please enter 1, 2 or 3");
        while (gameend) {
            while (player1) {
                System.out.print("Player 1 (X) enter row number.....: ");
                row = reader.nextInt();
                System.out.print("Player 1 (X) enter column number..: ");
                col = reader.nextInt();
                if (0 < row && row < 4 && 0 < col && col < 4) {
                    if (board[row - 1][col - 1] == ' ') {
                        board[row - 1][col - 1] = 'X';
                        player1 = false;
                        PrintBoard(board);
                        gameend = GameEnd(board);
                        winner = CheckBoard(board);
                        if (winner) {
                            System.out.println("!!!...Player 1 (X) wins...!!!");
                            System.out.println("!!!... CONGRATULATIONS ...!!!");
                            gameend = false;
                        }
                    } else {
                        System.out.println("Coordinate you entered isn't empty...!!!");
                    }
                } else {
                    System.out.println("Please enter 1, 2 or 3");
                }
            }

            while (player2 && gameend) {
                System.out.print("Player 2 (O) enter row number.....: ");
                row = reader.nextInt();
                System.out.print("Player 2 (O) enter column number..: ");
                col = reader.nextInt();
                if (0 < row && row < 4 && 0 < col && col < 4) {
                    if (board[row - 1][col - 1] == ' ') {
                        board[row - 1][col - 1] = 'O';
                        player2 = false;
                        PrintBoard(board);
                        gameend = GameEnd(board);
                        winner = CheckBoard(board);
                        if (winner) {
                            System.out.println("!!!...Player 2 (O) wins...!!!");
                            System.out.println("!!!... CONGRATULATIONS ...!!!");
                            gameend = false;
                        }
                    } else {
                        System.out.println("Coordinate you entered isn't empty...!!!");
                    }
                } else {
                    System.out.println("Please enter 1, 2 or 3");
                }

            }
            if (gameend) {
                player1 = true;
                player2 = true;
            }
        }
        if (!GameEnd(board)) {
            System.out.println("The game is a draw...!!!!");
        }
        reader.close();
        System.out.println("............made by mADEMatik");
    }

    public static boolean CheckBoard(char[][] board) {
        StringBuilder RowLine;
        StringBuilder ColLine;
        String Cross1 = "";
        String Cross2 = "";
        for (int i = 0; i < 3; i++) {
            RowLine = new StringBuilder();
            ColLine = new StringBuilder();
            for (int j = 0; j < 3; j++) {
                RowLine.append(board[i][j]);
                ColLine.append(board[j][i]);
                if (i == j) {
                    Cross1 += board[i][j];
                }
                if (i == 2 - j) {
                    Cross2 += board[i][j];
                }

            }
            if (RowLine.toString().equals("XXX") || RowLine.toString().equals("OOO")) {
                return true;
            }
            if (ColLine.toString().equals("XXX") || ColLine.toString().equals("OOO")) {
                return true;
            }
        }

        if (Cross1.equals("XXX") || Cross1.equals("OOO")) {
            return true;
        }
        return Cross2.equals("XXX") || Cross2.equals("OOO");
    }

    public static void PrintBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("  -------------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");
            }
            System.out.println();
            System.out.println("  -------------");
        }
    }

    public static boolean GameEnd(char[][] board) {
        int gameend = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == ' ') {
                    gameend += 1;
                }
            }
        }
        return gameend != 0;
    }
}