//Adem VAROL - 210709078

import java.io.IOException;
import java.util.Scanner;

public class TicTacToe_E1 {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
        int row, col;
        boolean player1 = true, player2 = true, gameend = true;

        while (gameend) {
            while (player1) {
                System.out.print("Player 1 (X) enter row number   :");
                row = reader.nextInt();
                System.out.print("Player 1 (X) enter column number:");
                col = reader.nextInt();
                if (0 < row && row < 4 && 0 < col && col < 4) {
                    if (board[row - 1][col - 1] == ' ') {
                        board[row - 1][col - 1] = 'X';
                        player1 = false;
                        PrintBoard(board);
                        gameend = GameEnd(board);
                    } else {
                        System.out.println("Coordinate you entered isn't empty...!!!");
                    }
                } else {
                    System.out.println("Please enter 1, 2 or 3");
                }
            }

            while (player2 && gameend) {
                System.out.print("Player 2 (O) enter row number   :");
                row = reader.nextInt();
                System.out.print("Player 2 (O) enter column number:");
                col = reader.nextInt();
                if (0 < row && row < 4 && 0 < col && col < 4) {
                    if (board[row - 1][col - 1] == ' ') {
                        board[row - 1][col - 1] = 'O';
                        PrintBoard(board);
                        gameend = GameEnd(board);
                        player2 = false;
                    } else {
                        System.out.println("Coordinate you entered isn't empty...!!!");
                    }
                } else {
                    System.out.println("Please enter 1, 2 or 3");
                }
            }
            if (gameend) {
                player1 = true;
                player2 = true;
            }
        }
        reader.close();
    }

    public static void PrintBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");
            }
            System.out.println();
            System.out.println("   -----------");
        }
    }

    public static boolean GameEnd(char[][] board) {
        int gameend = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == ' ') {
                    gameend += 1;
                }
            }
        }
        return gameend != 0;
    }
}