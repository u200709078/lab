//Adem VAROL - 200709078
package tictactoe;

public class InvalidMoveException extends Exception {
    public InvalidMoveException(String message) {
        super(message);
    }
}
