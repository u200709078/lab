public class DenemeBranch {
    public static void main(String[] args) {
        System.out.println("Prime numbers up to " + args[0] + ":"); //Program arguments[0]
        int mx = Integer.parseInt(args[0]);
        //Print the Prime Numbers that are less then mx
        //1. For each i less then mx
        for (int i = 2; i < mx; i++) {
            //1.1 Check the i is prime
            int dvr = 2; // Let divisor = 2
            boolean isPrime = true; // Let isPrime = true;
            while (dvr <= (i / 2) && isPrime) {  // While dvr is less than or equal to i/2 and isPrime is True
                if (i % dvr == 0) { //If the i is divisible by dvr
                    isPrime = false;//isPrime = false
                }
                dvr++; //Increment dvr;
            }
            if (isPrime) { //1.2 If the i is prime
                System.out.print(i + ","); //1.2.1 Print prime i
            }
        }
    }
}
